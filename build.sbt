name := "net.rosca.akka.bitfinexClientExample"

version := "1.0"

scalaVersion := "2.11.8"


libraryDependencies ++= {
  Seq(
     "org.slf4j" % "slf4j-simple" % "1.6.2"
    // Akka
    , "com.typesafe.akka" %% "akka-actor" % "2.4.9"
    //JSON parser
    , "org.json4s" %% "json4s-native" % "3.4.0"
    , "org.json4s" %% "json4s-jackson" % "3.4.0"
    //WebSockets client
    ,"com.ning" % "async-http-client" % "1.9.39"

  )
}