package net.rosca.akka.bitfinexClientExample

import com.ning.http.client.AsyncHttpClient
import com.ning.http.client.ws.{WebSocket, WebSocketTextListener, WebSocketUpgradeHandler}
import org.json4s.JsonAST.{JArray, JObject}
import org.json4s.jackson.JsonMethods._

class BitfinexExchangeAdapter(exchangeApiUrl: String, onTicker: (PairUpdate) => Unit) {
  var channelMap = Map.empty[Int, String]

  private val webSocket = {
    val wsClient = new AsyncHttpClient
    wsClient.prepareGet(exchangeApiUrl).execute(new WebSocketUpgradeHandler.Builder().addWebSocketListener(
      new WebSocketTextListener {

        override def onMessage(message: String): Unit = {
          processMessage(message)
        }

        override def onError(throwable: Throwable): Unit = {
          //TODO: error handling
        }

        override def onClose(webSocket: WebSocket){}
        override def onOpen(webSocket: WebSocket){}
      }
    ).build()).get
  }

  val pairs = Seq("btcusd", "ltcusd", "ltcbtc", "ethusd", "ethbtc")

  for (pair <- pairs)
    webSocket.sendMessage(s"""{"event": "subscribe", "channel": "ticker", "pair": "$pair"}""")

  def processMessage(rawMessage: String): Unit = {
    implicit val formats = org.json4s.DefaultFormats
    val message = parse(rawMessage)

    message match {
      case response: JObject =>
        (response \ "event").extract[String] match {
          case "subscribed" =>
            val pair = (response \ "pair").extract[String]
            val channelId = (response \ "chanId").extract[Int]

            channelMap += (channelId -> pair)
          case _ =>
        }
      case snapshot: JArray =>
        if (snapshot.values.size > 2) //ignore heartbeat messages
        {
          val channelId = snapshot(0).extract[Int]
          val pair = channelMap(channelId)

          val priceLast = snapshot(7).extract[Double]

          onTicker(PairUpdate(pair, priceLast))
        }
      case _ =>
    }
  }
}
