package net.rosca.akka.bitfinexClientExample

import akka.actor.{ActorSystem, Props}

object Main extends App {
  val system = ActorSystem("bitfinexExample")
  system.actorOf(Props(classOf[ExchangeClient]), "exchangeClient")
}
