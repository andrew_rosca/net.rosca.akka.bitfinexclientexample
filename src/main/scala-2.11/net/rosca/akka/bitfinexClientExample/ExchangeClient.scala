package net.rosca.akka.bitfinexClientExample

import akka.actor.{Actor, ActorRef, Props}

case class PairUpdate(pair: String, priceLast: Double)

class ExchangeClient extends Actor {
  val client = new BitfinexExchangeAdapter("wss://api2.bitfinex.com:3000/ws", onAdapterMessage)
  var pairTrackers = Map.empty[String, ActorRef]

  def onAdapterMessage(pairUpdate: PairUpdate): Unit ={
    self ! pairUpdate
  }

  override def receive: Receive = {
    case update: PairUpdate =>
      println(s"${self.path} message received: $update")

      def currentPair = update.pair

      pairTrackers.get(currentPair) match {
        case Some(pairTracker) => pairTracker ! update
        case None =>
          val newTracker = context.actorOf(Props(classOf[PairTracker], currentPair), s"pairTracker_$currentPair")
          pairTrackers += (currentPair -> newTracker)
          newTracker ! update
      }
  }
}
