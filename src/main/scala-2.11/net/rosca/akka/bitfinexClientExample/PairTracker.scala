package net.rosca.akka.bitfinexClientExample

import akka.actor.Actor

class PairTracker(pair: String) extends Actor {
  var priceMin = Double.MaxValue
  var priceMax = Double.MinValue
  var total = 0d
  var count = 0

  def avg = if (count != 0) total / count else Double.NaN

  override def receive = {
    case PairUpdate(currencyPair, priceLast) =>
      println(s"${self.path} last price received: $priceLast")

      total += priceLast
      count += 1

      priceMin = priceMin min priceLast
      priceMax = priceMax max priceLast

      println(f"$pair%-10s last: $priceLast%-10.4f high: $priceMax%-10.4f low: $priceMin%-10.4f avg: $avg%-10.4f")
  }
}
